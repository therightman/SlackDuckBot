from slackconnector import SlackConnector
import config
import logging
import coloredlogs
import util
import re


def main():

    s = SlackConnector(config.SLACK_TOKEN, type_filter=["message"])

    for event in s.read():
        logging.debug("%s", event)

        if "subtype" not in event and event["user"] != config.BOT_NAME:
            channel = event["channel"]
            user = util.getUserdataForId(event["user"])

            message = event["text"]
            trigger = message.split()[0].lower()
            trig_message = re.sub('^\S+\s+', '', message)

            for plugin in config.PLUGINS:
                logging.debug("Plugin %s handling message", plugin.__class__.__name__)

                if plugin.trigger_handler and plugin.trigger_handler.activated(trigger):
                        plugin.on_trigger(channel, user, trig_message, trigger[1:], s)
                else:
                    if "@" + config.BOT_NAME in util.replaceIdsInMessage(message):
                        plugin.on_mention(channel, user, message, s)
                    plugin.on_message(channel, user, message, s)

if __name__ == '__main__':
    import logging.config
    # logging.config.fileConfig('/path/to/logging.conf', disable_existing_loggers=False)
    config.PLUGINS = [x() for x in config.PLUGINS]  # init all plugins
    logging.basicConfig(level=logging.DEBUG)
    coloredlogs.install(level='DEBUG')
    main()
