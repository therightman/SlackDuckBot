import re
import config
import requests
import hashlib
import time
import hmac
import base64

from plugin import Plugin
from trigger_handler import TriggerHandler

LANGUAGES = ("python3", "python", "ruby", "php", "java", "go", "nodejs", "csharp", "fsharp", "cpp", "cpp11", "c")

codeblock_regex = re.compile(r'(```|`)(.+)\1', re.DOTALL)


class Replit(Plugin):
    """Usage: `eval LANGUAGE CODE`. Runs code via repl.it api and posts the results. Please put code between backticks."""

    def _gen_token(self):
        time_created = int(round(time.time() * 1000))

        dig = hmac.new(config.REPLIT_API_KEY, msg=str(time_created).encode("utf-8"), digestmod=hashlib.sha256).digest()
        mac = base64.b64encode(dig).decode()

        return str(time_created) + ":" + mac

    def _eval_code(self, language, code):
        payload = {"auth": self._gen_token(), "language": language, "code": code}
        req = requests.post("https://api.repl.it/eval", data=payload)

        if req.status_code == requests.codes.ok:
            result = req.json()[0] #TODO Optimize this
            if result["data"] != "None" and result["error"] == "":
                return self.concat_json(req.json())
            else:
                return result["error"]
        else:
            return "Something went wrong while trying to run your code. Try again later."

    def concat_json(self, data):
        return "".join([x["data"] for x in data if x["data"] != 'None'])


    def __init__(self):
        self.trigger_handler = TriggerHandler("eval")

    def on_trigger(self, channel, user, message, trigger, s):

        if len(message.split()) >= 2:
            language = message.split()[0].lower()
            if language in LANGUAGES:
                regex_search = codeblock_regex.search(message)
                if regex_search is not None:
                    code = regex_search.group(2).strip()
                    s.start_typing(channel)
                    output = self._eval_code(language, code)

                    if output == "":
                        output = "No output"

                    s.send_msg("```" + output + "```", channel)
                else:
                    s.send_msg("Code not found, please put it in a codeblock (backticks)", channel)
            else:
                s.send_msg("Unrecognized language, valid languages are: " + ", ".join(LANGUAGES), channel)
        else:
            s.send_msg("Not enough arguments for repl, format is !eval LANGUAGE `CODE`", channel)

