import random
import db
import config
import markovify
from plugin import Plugin
from trigger_handler import TriggerHandler
from sqlalchemy import Table, Column, Integer, MetaData, Text


class Markov(Plugin):
    """Usage: `markov`. Requires SimpleLogger plugin to build a corpus. The command triggers the generation of a random sentence using markov chains. Can also post randomly if set."""

    def __init__(self):
        super(self.__class__, self).__init__()
        self.trigger_handler = TriggerHandler("markov")

        metadata = MetaData()

        self.textarchive_table = Table('textarchive', metadata,
            Column('pk', Integer, primary_key=True),
            Column('message', Text),
        )

        metadata.create_all(db.get_engine())

        try:
            self.RANDOM_MARKOV = config.RANDOM_MARKOV
            # or getattr(self, 'big_object')
        except AttributeError:
            self.RANDOM_MARKOV = False

    def createSentence(self):
        corpus = None

        with db.get_engine().begin() as conn:
            result = conn.execute(self.textarchive_table.select())
            rows = result.fetchall()
            corpus = "\n".join([x[1] for x in rows])

        if corpus:
            text_model = markovify.NewlineText(corpus, state_size=3)
            sentence = text_model.make_sentence(tries=100)

            return sentence

    def createLongerSentence(self, length=140):
        results = None

        with db.get_engine().begin() as conn:
            result = conn.execute(self.textarchive_table.select())
            rows = result.fetchall()
            corpus = "\n".join([x[1] for x in rows])

        if corpus:
            text_model = markovify.NewlineText(results, state_size=3)
            sentence = text_model.make_short_sentence(length, tries=100)

            return sentence

    def on_trigger(self, channel, user, message, trigger, s):
        s.start_typing(channel)
        s.send_msg(self.createSentence(), channel)

    def on_message(self, channel, user, message, s):
        if random.random() < 0.005 and self.RANDOM_MARKOV:
            s.start_typing(channel)
            s.send_msg(self.createSentence(), channel)

    def on_mention(self, channel, user, message, s):
        s.start_typing(channel)
        s.send_msg(self.createSentence(), channel)

if __name__ == '__main__':
    plug = Markov()
    print(plug.createLongerSentence())